README.md

# initiate minikube
minikube start

# apply files to minikube
kubectl apply -f pvflask.yml

# allow minikube to expose external IP
kubectl expose deploy nginx --port 80 --type NodePort -n flask-service

# find external ip address using another terminal
kubectl get svc -n flask-service

# access PhpMyAdmin
Open browser to external IP on port 80
